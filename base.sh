#!/usr/bin/bash
up=0
down=0

list=$1
logfile=$2
scpdir="$(dirname "$(readlink -f "$0")")"

if [ ! -f "$list" ]; then
  if [ ! -f "${scpdir}/containerlist" ];then
    touch "${scpdir}/containerlist"
  fi
  list="${scpdir}/containerlist"
fi
if [ "$logfile" = "" ]; then
  logfile="${scpdir}/log.log"
fi


while true;
do
  if ping -q -c 1 -W 1 8.8.8.8 &> /dev/null; then

    echo "IPv4 is up"

    #do something and use && to set up=1

    if [ $up -eq 1 ];  then
      echo -e UP '\t|\t' $(date) '\t|\t'  >> $logfile
      up=0
      upcont=''
    fi
  else

    echo "IPv4 is down"

    #do something and use && to set down=1

    if [ $down -eq 1 ]; then
      echo -e DOWN '\t|\t' $(date) '\t|\t'  >> $logfile
      down=0
    fi
  fi
  sleep 10
done
