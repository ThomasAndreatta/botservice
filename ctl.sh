#!/usr/bin/bash
up=0
upcont=''
down=0
downcont=''


#command="bash /etc/bot/" # + text in the containerlist file
command="docker run --rm  --detach "

list=$1
logfile=$2
scpdir="$(dirname "$(readlink -f "$0")")"
if [ ! -f "$list" ]; then
  if [ ! -f "${scpdir}/containerlist" ];then
    touch "${scpdir}/containerlist"
  fi
  list="${scpdir}/containerlist"
fi
if [ "$logfile" = "" ]; then
  logfile="${scpdir}/log.log"
fi

echo $logfile

while true;
do
  if ping -q -c 1 -W 1 8.8.8.8 &> /dev/null; then
    #echo "IPv4 is up"

    while read p; do
      [ ! "$(docker container ls | grep $p)" ]  &> /dev/null && $command''$p && up=1 && upcont="${upcont} $p" &> /dev/null
    done < $list
    if [ $up -eq 1 ];  then
      echo -e UP '\t|\t' $(date) '\t|\t' $upcont >> $logfile
      up=0
      upcont=''
    fi
  else
    #echo "IPv4 is down"

    for i in $(docker ps --format "{{.Names}}"); do

      tmp=$(docker ps -f name=$i --format '{{.Image}}')
      docker stop $i &> /dev/null
      downcont="${downcont} ${tmp}"
      #echo 'stopped' $i
      down=1
    done
    if [ $down -eq 1 ]; then
      echo -e DOWN '\t|\t' $(date) '\t|\t' $downcont >> $logfile
      down=0
      downcont=''
    fi
  fi
  sleep 10
done
