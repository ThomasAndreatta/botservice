#!/usr/bin/bash
usage() { echo "Usage: $0 [-l <log path>] [-c <container name file path>]" 1>&2; exit 1; }


while getopts ":l:c:" o; do
    case "${o}" in
        c)
            containerlist=${OPTARG}
            ;;
        l)
            logpath=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done



dir="$(dirname "$(readlink -f "$0")")"
scriptpath="${dir}/ctl.sh"
chmod +x ctl.sh




echo "[Unit]" > bot.service
echo "Description=docker service" >> bot.service
echo "After=network.target" >> bot.service
echo "StartLimitIntervalSec=0" >> bot.service
echo "[Service]" >> bot.service
echo "Type=simple" >> bot.service
echo "Restart=always" >> bot.service
echo "RestartSec=1" >> bot.service
echo "User=$(whoami)" >> bot.service
echo "ExecStart=$(which bash) $scriptpath \"$containerlist\" \"$logpath\"" >> bot.service
echo "" >> bot.service
echo "[Install]" >> bot.service
echo "WantedBy=multi-user.target" >> bot.service

sudo cp bot.service /etc/systemd/system/
sudo systemctl enable bot.service
