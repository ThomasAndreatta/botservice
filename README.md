# AUTORUN when OFFLINE/ONLINE
Once executed "install.sh" a systemctl service will be created with "ctl.sh" as target.
ctl.sh automatically run a command when the server switch from online->offline or viceversa.

ctl.sh is configured for docker container control:
turn on the container in the file "containerlist" when ONLINE
turn off ALL the containers when the sever is OFFLINE

if you have some particular setting for the container you can modify the var "command" and the script will run that
\- adding the container name from the containerlist file  - instead of the default command

The base.sh file work in the same way but it's not set for docker stuff, you can simply add your commands, it will write
log in the same way as the other one.

If you move the ctl.sh file once installed you'll need to modify the "/etc/systemd/system/bot.service" file with the new ctl.sh path.
You can add docker container (or commands, depends from how you've modified the ctl.sh file) to the containerlist whenever you want
without restarting the service, same for changes in the ctl.sh code.

---
# install.sh Usage
```bash
./install.sh #install the service using this folder as default for the log logfile
#and for the containerlist file, if this is not present it will create it and run with an empty file.
```

```bash
./install.sh -l "/path/to/logfile" #set the path to the logfile
```
```bash
./install.sh -c "/path/to/containerlist" #set the path to the containerlist
```
```bash
./install.sh -c "/path/to/containerlist" -l "/path/to/logfile" #set the path to the containerlist and logfile
```
